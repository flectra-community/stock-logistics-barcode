# Copyright (C) 2022 Akretion
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    "name": "Sale Input Barcode",
    "version": "2.0.1.0.1",
    "category": "Tools",
    "website": "https://gitlab.com/flectra-community/stock-logistics-barcode",
    "author": "Akretion, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "installable": True,
    "summary": "Add Sale line with barcode",
    "maintainers": ["bealdav"],
    "depends": [
        "sale_management",
        "barcode_action",
        "base_gs1_barcode",
    ],
    "data": [
        "views/sale.xml",
    ],
    "demo": [],
}
