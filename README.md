# Flectra Community / stock-logistics-barcode

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[stock_barcodes_gs1](stock_barcodes_gs1/) | 2.0.1.0.1| It provides read GS1 barcode on stock operations.
[barcodes_generator_abstract](barcodes_generator_abstract/) | 2.0.1.0.1| Generate Barcodes for Any Models
[barcodes_generator_product](barcodes_generator_product/) | 2.0.1.1.0| Generate Barcodes for Products (Templates and Variants)
[stock_inventory_barcode](stock_inventory_barcode/) | 2.0.1.0.0| Add simple barcode interface on inventories
[base_gs1_barcode](base_gs1_barcode/) | 2.0.1.0.1| Decoding API for GS1-128 (aka UCC/EAN-128) and GS1-Datamatrix
[barcodes_generator_location](barcodes_generator_location/) | 2.0.1.0.0| Generate Barcodes for Stock Locations
[product_supplierinfo_barcode](product_supplierinfo_barcode/) | 2.0.1.0.0| Add a barcode to supplier pricelist items
[stock_barcodes_gs1_expiry](stock_barcodes_gs1_expiry/) | 2.0.1.0.1| It provides read expiry dates from GS1 barcode on stock operations.
[stock_barcodes](stock_barcodes/) | 2.0.1.2.2| It provides read barcode on stock operations.
[sale_input_barcode](sale_input_barcode/) | 2.0.1.0.1| Add Sale line with barcode
[product_multi_barcode](product_multi_barcode/) | 2.0.1.0.1| Multiple barcodes on products
[stock_barcodes_automatic_entry](stock_barcodes_automatic_entry/) | 2.0.1.0.0|         This module will automatically trigger the click event on a button        with the class 'barcode-automatic-entry' after a barcode scanned has        been processed.    


